// CRUD Operations
/*
    - CRUD operations are the heart of any backend application.

// Finding a single document
// Leaving the search criteria empty will retrieve ALL the documents
db.users.find();
db.users.find({query}, {projection});

db.users.find({ firstName: "Stephen" });
db.users.find(
    { 
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76
    }
);

// The "pretty" method allows us to be able to view the documents returned by our terminals in a better format
    // Note that this is mostly applicable in terminals and older versions of robo3t (1.1 and older)
    - Syntax
        - db.collectionName.find({ fieldA: valueA, fieldB: valueB });
*/
db.users.find({ lastName: "Armstrong", age: 82 })
db.users.find({ lastName: "Armstrong", age: 82 })


// Finding documents with the use of query comparison operator
db.users.find(
    {
        age: { $gt: 20 }
    }
);

db.users.find(
    {
        age: { $eq: 75 }
    }
);

db.users.find(
    {
        age: 75
    }
);

db.users.find(
    {
        lastName: { $in: ["Doe", "Armstrong", "Hawking"] }
    }
)

// Finding documents with the use of query logical operator

    //using and logical operator
db.users.find(
    {
        $and: [
            { firstName: "Jane" },
            { lastName: "Doe" }
        ]
    }
)

db.users.find(
    {
        firstName: "Jane",
        lastName: "Doe"
    }
)

    // using or logical operator
db.users.find(
    {
        $or: [
            { firstName: "Stephen" },
            { lastName: "Doe" },
            { age: {$gte: 80} }
        ]
    }
)


    // using regex operator
db.users.find(
    {
        firstName: { $regex: "EI", $options: "i" }
    }
)


// Finding documents with the use of query and field projection
    // specify 1 or 0 if you wish to include or exclude the fields of a document
db.users.find(
    {
        _id: ObjectId("624ffc72895b8af0aca5317d")
    },
    {
        firstName: 1,
        age: 1,
        _id: 0
    }
)

// Look for a document that has a firstName of Neil and display only the first name and phone
db.users.find(
    {
        firstName: "Neil"
    },
    {
        firstName: 1,
        contact: {phone: 1},
        _id: 0
    }
)


// Update Operation
    // Update operations modify existing documents in a collection. MongoDB provides the following methods to update documents of a collection

    // db.collections.updateOne({filter}, {update})
    // db.collections.updateMany()
    // db.collections.replaceOne()

    //we will insert a dummy document to be able to use for update operation
    db.users.insertOne(
        {
            firstName: "Test",
            lastName: "Test",
            age: 0,
            contact: {
                phone: "0",
                email: "test@mail.com"
            },
            department: "none"
        }
    )

db.users.updateOne(
    {
        firstName: "Test"
    },
    {
        $set: {
            firstName: "Joy",
            lastName: "Pague",
            age: 16,
            contact: {
                phone: "12345678910",
                email: "joy@mail.com"
            },
            department: "none"
        }
    }
)

// update the newly updated document with department to Instructor Department
db.users.updateOne(
    {
        firstName: "Joy"
    },
    {
        $set: {
            department: "Instructor"
        }
    }
)

// remove the specified field using $unset update operator
db.users.updateOne(
    {
        firstName: "Joy"
    },
    {
        $unset: {
            department: "Instructor"
        }
    }
)

// What if I got multiple documents from the filter paramter and I only want to update one document
    //we will insert a dummy documents to be able to use for update operation
    db.users.insertMany(
        [
            {
                firstName: "Test",
                lastName: "Test",
                age: 0,
                contact: {
                    phone: "0",
                    email: "test@mail.com"
                },
                department: "none"
            },
            {
                firstName: "Test",
                lastName: "Test",
                age: 0,
                contact: {
                    phone: "0",
                    email: "test@mail.com"
                },
                department: "none"
            }
        ]
    )

//using update one to update these newly added documents

db.users.updateOne(
    {
        firstName: "Test"
    },
    {
        $set: {
            firstName: "Hannah",
            lastName: "Tapay",
            age: 18,
            contact: {
                phone: "12345678910",
                email: "hannah@mail.com"
            },
            department: "none"
        }
    }
)


// Find all documents with department field and update all documents to HR department
db.users.updateMany( 
    {  
        department: {$exists: true}
 
    },
    { 
       $set:{ 
            department : "HR"
        }
    }
 );



// db.collections.replaceOne({filter}, {replacement})
    // replaces a single document
db.users.replaceOne(
    {
        firstName: "Test"
    },
    {
        firstName: "Johnny",
        lastName: "Cuyno"
    }
)



//removes specific field using $unset  update operator

db.users.updateOne(
    {
        firstName: "Cris"
    },
    {
        $unset: {
        department: "Instructor Department"
        }
    }
);

//what if there are multiple docs from filder parameter and only needs to update 1 doc?

db.users.insertMany(
 [
	{
		firstName: "Test",
		lastName: "Test",
		age: 0,
		contactInfo: {
			phone: "0",
			email: "test@mail.com"
		},
		department: "none"
	},	
	{
		firstName: "Test",
		lastName: "Test",
		age: 0,
		contactInfo: {
			phone: "0",
			email: "test@mail.com"
		},
		department: "none"
	}
 ]
);


//this will update the first input data in the update many

db.users.updateOne(

	{
		firstName: "Test",
	},
    {
        $set: {
            firstName: "Anna",
            lastName: "Cristobal",
            age: 18,
            contactInfo: {
			  	phone: "111111111111",
			  	email: "anna@mail.com"
                },
        	department: "none"
            }
	}	
);

===========================================================
//find all docs w dept field & update all docs to HR Dept

db.users.updateMany(
    { department: { $exists: true } },
    { $set: { department: "HR Department" } }
);

//replaceOne
	//replaces single doc w/in collection
	//db.collections.replaceOne()
//======CHECK THIS AGAIN......======
db.collection.replaceOne(
   {
	firstName: "Test"   	
   },
   {
     firstName: "Johnny",
     lastName: "Cuyno"
     }
);
==========================================================
// Delete Operations
    // Delete operations remove documents from a collection. MongoDB provides the following methods to delete documents of a collection:

    // db.collections.deleteOne({filter})
    // db.collections.deleteMany({filter})

    //we will insert a dummy documents to be able to use for update operation
    db.users.insertOne(
        {
            firstName: "Test",
            lastName: "Test",
            age: 0,
            contact: {
                phone: "0",
                email: "test@mail.com"
            },
            department: "none"
        }
    );
    


//deleteOne() to delete a single document

db.users.deleteOne({firstName: "Test"});

 // inserting dummy documents for deleteMany() method
    db.users.insertMany(
        [
            {
                firstName: "Test",
                lastName: "Test",
                age: 0,
                contact: {
                    phone: "0",
                    email: "test@mail.com"
                },
                department: "none"
            },
            {
                firstName: "Test",
                lastName: "Test",
                age: 0,
                contact: {
                    phone: "0",
                    email: "test@mail.com"
                },
                department: "none"
            }
        ]
    )

//delete multiple entries
db.users.deleteMany({lastName: "Test"});

//delete multi[ple docs targetting same field w diff values

db.users.deleteMany(
 {
	firstName: {$n: ["Joy", "Hannah", "Johnny"]}
 }
)